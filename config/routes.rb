Rails.application.routes.draw do

  resources :divisa_valors
  resources :divisas

  resources :categoris 
  resources :productos
  resources :users

  devise_for :usuarios
  root 'productos#index'
 


  resources :registros
  post 'registro', to: 'registros#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
