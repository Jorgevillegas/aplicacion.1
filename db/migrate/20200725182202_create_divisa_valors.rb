class CreateDivisaValors < ActiveRecord::Migration[6.0]
  def change
    create_table :divisa_valors do |t|

      t.date :fecha
      t.float :valor
      

      t.timestamps
    end
  end
end
