class RemoveCategoriaFromProductos < ActiveRecord::Migration[6.0]
  def change
    remove_column :productos, :categoria, :string
  end
end
