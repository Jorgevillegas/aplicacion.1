class ChangeColumnInTable < ActiveRecord::Migration[6.0]
	def up
	  change_column :productos, :precio, :float
	end

	def down
	  change_column :productos, :precio, :integer
	end
end
