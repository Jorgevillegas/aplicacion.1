class RemoveColumn < ActiveRecord::Migration[6.0]
  def change
  	remove_column :divisa_valors, :divisa_id, :integer
  	remove_column :divisa_valors, :divisa_valors_id, :integer
  end
end
