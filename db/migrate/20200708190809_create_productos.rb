class CreateProductos < ActiveRecord::Migration[6.0]
  def change
    create_table :productos do |t|
      t.string :nombre
      t.string :categoria
      t.integer :cantidad_cajas
      t.integer :cantidad_x_display
      t.text :sabores
      t.integer :precio
      t.timestamps
    end
  end
end
#rails generate migration AddPrecioToProductos precio