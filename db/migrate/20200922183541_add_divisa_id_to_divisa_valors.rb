class AddDivisaIdToDivisaValors < ActiveRecord::Migration[6.0]
  def change
    add_reference :divisa_valors, :divisa, null: false, foreign_key: true
  end
end
