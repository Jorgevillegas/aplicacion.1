require "application_system_test_case"

class DivisaValorsTest < ApplicationSystemTestCase
  setup do
    @divisa_valor = divisa_valors(:one)
  end

  test "visiting the index" do
    visit divisa_valors_url
    assert_selector "h1", text: "Divisa Valors"
  end

  test "creating a Divisa valor" do
    visit divisa_valors_url
    click_on "New Divisa Valor"

    fill_in "Divisa", with: @divisa_valor.divisa_id
    fill_in "Fecha", with: @divisa_valor.fecha
    fill_in "Valor", with: @divisa_valor.valor
    click_on "Create Divisa valor"

    assert_text "Divisa valor was successfully created"
    click_on "Back"
  end

  test "updating a Divisa valor" do
    visit divisa_valors_url
    click_on "Edit", match: :first

    fill_in "Divisa", with: @divisa_valor.divisa_id
    fill_in "Fecha", with: @divisa_valor.fecha
    fill_in "Valor", with: @divisa_valor.valor
    click_on "Update Divisa valor"

    assert_text "Divisa valor was successfully updated"
    click_on "Back"
  end

  test "destroying a Divisa valor" do
    visit divisa_valors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Divisa valor was successfully destroyed"
  end
end
