require 'test_helper'

class DivisaValorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @divisa_valor = divisa_valors(:one)
  end

  test "should get index" do
    get divisa_valors_url
    assert_response :success
  end

  test "should get new" do
    get new_divisa_valor_url
    assert_response :success
  end

  test "should create divisa_valor" do
    assert_difference('DivisaValor.count') do
      post divisa_valors_url, params: { divisa_valor: { divisa_id: @divisa_valor.divisa_id, fecha: @divisa_valor.fecha, valor: @divisa_valor.valor } }
    end

    assert_redirected_to divisa_valor_url(DivisaValor.last)
  end

  test "should show divisa_valor" do
    get divisa_valor_url(@divisa_valor)
    assert_response :success
  end

  test "should get edit" do
    get edit_divisa_valor_url(@divisa_valor)
    assert_response :success
  end

  test "should update divisa_valor" do
    patch divisa_valor_url(@divisa_valor), params: { divisa_valor: { divisa_id: @divisa_valor.divisa_id, fecha: @divisa_valor.fecha, valor: @divisa_valor.valor } }
    assert_redirected_to divisa_valor_url(@divisa_valor)
  end

  test "should destroy divisa_valor" do
    assert_difference('DivisaValor.count', -1) do
      delete divisa_valor_url(@divisa_valor)
    end

    assert_redirected_to divisa_valors_url
  end
end
