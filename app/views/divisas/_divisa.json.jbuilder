json.extract! divisa, :id, :nombre, :status, :created_at, :updated_at
json.url divisa_url(divisa, format: :json)
