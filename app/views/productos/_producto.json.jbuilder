json.extract! producto, :id, :nombre, :categoria, :cantidad_cajas, :cantidad_x_display, :sabores, :created_at, :updated_at
json.url producto_url(producto, format: :json)
