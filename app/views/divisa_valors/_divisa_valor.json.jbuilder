json.extract! divisa_valor, :id, :divisa_id, :fecha, :valor, :created_at, :updated_at
json.url divisa_valor_url(divisa_valor, format: :json)
