class Producto < ApplicationRecord

	belongs_to :categori

	has_one_attached :image

	validates :nombre, presence: true, uniqueness: true
	#validates :categoria, presence: true
	validates :cantidad_x_display, presence: true	
	validates :precio, presence: true
	validates :categori, presence: true


	def update_Precios
		if precio > 500
			
		self.update(precio: ((precio)/(DivisaValor.last.valor)))
		self.update(PrecioUnidadBs: ((precio)*(DivisaValor.last.valor))/cantidad_x_display)

		else

		self.update(PrecioUnidadBs: ((precio)*(DivisaValor.last.valor))/cantidad_x_display)

		end
	end	


end
