class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]

  # GET /productos
  # GET /productos.json
  def index
    @productos = Producto.all.sort_by{ |m| m.nombre.downcase }
    @productos
    @q = Producto.ransack(params[:q])
    @productos = @q.result.includes(:categori).uniq
  end

  # GET /productos/1
  # GET /productos/1.json
  def show

  end

  # GET /productos/new
  def new
    @producto = Producto.new
  end

  # GET /productos/1/edit
  def edit
  end

  # POST /productos
  # POST /productos.json
  def create
    @producto = Producto.new(producto_params)

    respond_to do |format|
      if @producto.save
        @producto.update_Precios
        format.html { redirect_to :action=>"index", :controller=>"productos", notice: 'Producto registrado satisfactoriamente.' }
        format.json { render :show, status: :created, location: @producto }
      else
        format.html { render :new }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /productos/1
  # PATCH/PUT /productos/1.json
  def update
    respond_to do |format|
      if @producto.update(producto_params)
        @producto.update_Precios
        format.html { redirect_to :action=>"index", :controller=>"productos", notice: 'Producto satisfactoriamente actualizado.' }
        format.json { render :show, status: :ok, location: @producto }
      else
        format.html { render :edit }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /productos/1
  # DELETE /productos/1.json
  def destroy
    @producto.destroy
    respond_to do |format|
      format.html { redirect_to productos_url, notice: 'Producto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def producto_params
      params.require(:producto).permit(:image, :nombre, :categori_id, :cantidad_cajas, :cantidad_x_display, :sabores, :precio , :PrecioUnidadBs , :PrecioCajaBs)
    end
end
