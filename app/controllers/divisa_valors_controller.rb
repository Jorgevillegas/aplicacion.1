class DivisaValorsController < ApplicationController
  before_action :set_divisa_valor, only: [:show, :edit, :update, :destroy]

  # GET /divisa_valors
  # GET /divisa_valors.json
  def index
    @divisa_valors = DivisaValor.all
  end

  # GET /divisa_valors/1
  # GET /divisa_valors/1.json
  def show
  end

  # GET /divisa_valors/new
  def new
    @divisa_valor = DivisaValor.new
    #@divisas = Divisa.all
  end

  # GET /divisa_valors/1/edit
  def edit
  end

  # POST /divisa_valors
  # POST /divisa_valors.json
  def create
    @divisa_valor = DivisaValor.new(divisa_valor_params)

    respond_to do |format|
      if @divisa_valor.save
        format.html { redirect_to @divisa_valor, notice: 'Divisa valor was successfully created.' }
        format.json { render :show, status: :created, location: @divisa_valor }
      else
        format.html { render :new }
        format.json { render json: @divisa_valor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /divisa_valors/1
  # PATCH/PUT /divisa_valors/1.json
  def update
    respond_to do |format|
      if @divisa_valor.update(divisa_valor_params)
        format.html { redirect_to @divisa_valor, notice: 'Divisa valor was successfully updated.' }
        format.json { render :show, status: :ok, location: @divisa_valor }
      else
        format.html { render :edit }
        format.json { render json: @divisa_valor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /divisa_valors/1
  # DELETE /divisa_valors/1.json
  def destroy
    @divisa_valor.destroy
    respond_to do |format|
      format.html { redirect_to divisa_valors_url, notice: 'Divisa valor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_divisa_valor
      @divisa_valor = DivisaValor.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def divisa_valor_params
      params.require(:divisa_valor).permit(:divisa_id, :fecha, :valor)
    end
end
